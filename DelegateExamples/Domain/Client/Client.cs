﻿using System;

namespace Domain.Client
{
    public class Client
    {
        public Guid Id { get; set; } = new Guid();
        public string Name { get; set; }
    }
}
