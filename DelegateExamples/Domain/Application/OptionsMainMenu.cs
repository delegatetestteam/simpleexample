﻿namespace Domain.Application
{
    public enum OptionsMenu
    {
        ListClient = 1,
        NewClient = 2,
        CountClient = 3,
        CloseApp = 4
    }
}
