﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace ConsoleApplication
{
    static class Program
    {
        #region Attributes
        public static List<Domain.Client.Client> Clients = new List<Domain.Client.Client>();
        public static int QttClients = 0;
        #endregion

        #region Delegates
        delegate void ClientAddedHandler(Domain.Client.Client NewClient);
        #endregion

        #region Events
        static event ClientAddedHandler OnClientAdded;
        #endregion

        static void Main(string[] args)
        {
            Program.OnClientAdded += Program_OnClientAdded;
            ShowMenu();
        }

        private static void ShowMenu()
        {
            Console.WriteLine("Informe o código do que deseja fazer");
            Console.WriteLine("1 - Listar Clientes");
            Console.WriteLine("2 - Cadastrar Cliente");
            Console.WriteLine("3 - Contar Clientes");
            Console.WriteLine("4 - Fechar App");
            ReadSelectedMenuItem();
        }

        private static void ReadSelectedMenuItem()
        {
            try
            {
                Domain.Application.OptionsMenu option = (Domain.Application.OptionsMenu)int.Parse(Console.ReadLine());

                switch (option)
                {
                    case Domain.Application.OptionsMenu.ListClient:
                        ListClients();
                        break;
                    case Domain.Application.OptionsMenu.NewClient:
                        ReadNewClient();
                        break;
                    case Domain.Application.OptionsMenu.CountClient:
                        CountClient();
                        break;
                    case Domain.Application.OptionsMenu.CloseApp:

                        break;
                    default:
                        Message("Opção informada inválida!");
                        break;
                }
            }
            catch (FormatException)
            {
                Message("Opção informada inválida!");
            }
        }

        private static void CountClient()
        {
            Console.Clear();
            Console.WriteLine(string.Format("Exitem {0} clientes na base.", QttClients));
            Console.ReadKey();
            Console.Clear();
            ShowMenu();
        }

        private static void ReadNewClient()
        {
            Console.Clear();
            string name = "";

            while (string.IsNullOrWhiteSpace(name))
            {
                Console.WriteLine("Informe o nome do novo cliente...");
                name = Console.ReadLine();
            }

            Domain.Client.Client client = new Domain.Client.Client()
            {
                Name = name
            };

            Clients.Add(client);
            Program.OnClientAdded(client);


            Message("Cliente registrado!");
        }

        private static void Program_OnClientAdded(Domain.Client.Client NewClient)
        {
            QttClients++;
        }

        private static void ListClients()
        {
            Console.Clear();
            Console.WriteLine("*************** Início ***************");
            Clients.OrderBy(_ => _.Name).ToList().ForEach(_ => Console.WriteLine("Id: " + _.Id + " --- Nome: " + _.Name));
            Console.WriteLine("*************** Fim ******************");
            Console.WriteLine();

            Console.ReadKey();
            Console.Clear();
            ShowMenu();
        }

        private static void Message(string msg)
        {
            Console.Clear();
            Console.WriteLine();
            StringBuilder sb = new StringBuilder("**************************************** ");
            sb.Append(msg);
            Console.WriteLine(sb);
            ShowMenu();
        }

    }

}
